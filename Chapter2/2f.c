/*If three side sof a triangle are entered check whether the triangle is valid or not */

#include <stdio.h>
main(){

	int a,b,c,greatest;
	printf("Enter the three sides of the triangle : \n");
	scanf("%d %d %d",&a,&b,&c);

	greatest=a>b?(a>c?a:c):(b>c?b:c);
	printf("Greatest side %d \n",greatest);
	if(a+b>c && c==greatest|| a+c>b && b==greatest|| b+c>a && a==greatest)
		printf("Valid\n");
	else
		printf("Invalid triangle");
}

/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2f.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
5
6
12
Greatest side 12 
Invalid trianglepranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
15
5
10
Greatest side 15 
Invalid trianglepranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2f.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
12
6
8
Greatest side 12 
Valid
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
15
30
16
Greatest side 30 
Valid
*/