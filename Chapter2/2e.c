/*A library charges fine for every book returned late.Write a program to accept number of days the member is late to return the book and
display the appropriate message.*/
#include <stdio.h>
main()
{
	int days;
	printf("Enter the number of days by which the member is late to return the book : \n");
	scanf("%d",&days);

	if(days<=5)
		printf("Fine- 5p");
	else if(days>=6 && days<=10)
		printf("Fine- Rs 1");
	else if(days>=10 && days<=30)
		printf("Fine- Rs 5");
	else if(days>=30)
		printf("Membership cancelled");
}

/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2e.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the number of days by which the member is late to return the book : 
32
Membership cancelledpranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the number of days by which the member is late to return the book : 
11
Fine- Rs 5
*/

