/*Given the coordinates of the circle and radius. Find whether the point lies inside, outside or on the circle*/

#include <stdio.h>
#include <math.h>
main()
{
	int xr,yr,rad,x,y;
	float a;
	printf("Enter the coordinates of the center of the circle, radius and a point\n");
	scanf("%d %d %d %d %d",&xr,&yr,&rad,&x,&y);

	a=sqrt(pow(xr-x,2)+pow(yr-y,2));
	if(a==rad)
		printf("On the circle\n");
	else if(a>rad)
		printf("Outside the circle\n");
	else if(a<rad)
		printf("Inside the circle\n");
}

/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 1k.c -lm //note link with library for def of pow and sqrt
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the coordinates of the center of the circle, radius and a point
0
0
3
3
0
On the circle
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the coordinates of the center of the circle, radius and a point
0
0
3
6
1
Outside the circle
*/