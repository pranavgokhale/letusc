/*Any year is entered through the keyboard, write a program to determine whether the year is leap or not. Use the operators && and ||. */

#include <stdio.h>

main()
{
	int year;
	printf("Enter the year \n");
	scanf("%d",&year);
	if(year%400==0 || year%100!=0 && year%4==0)	//this is the condition of leap year..pls note (I didnt know the first two conditions :( ))
		printf("Leap year \n");
	else
		printf("Not a leap year ");
}

/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2a.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year 
1900
Not a leap year pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year 
2016
Leap year 
*/
