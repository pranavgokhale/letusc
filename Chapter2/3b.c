/*Write a program using conditional operators to determine whether a year entered through the keyboard is a leap year or not */

#include <stdio.h>

main()
{
	int year;
	printf("Enter the year :\n");
	scanf("%d",&year);

	year%400!=0?(year%100!=0?(year%4==0?printf("Leap year\n"):printf("Not leap\n")):printf("Not leap\n")):printf("Leap year\n");
}

/*Output
 pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 3b.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year :
2012
Leap year
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year :
1900
Not leap
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year :
2000
Leap year
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year :
1800
Not leap
*/