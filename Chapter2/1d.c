/*According to gregorian calendar, it was monday on the date 01/01/0001.If any year is input through the keyboard 
write a program to find out what is the day on 1st january of this year */

#include <stdio.h>
main(){

	long int tdays,ldays,ndays;
	int year;
	printf("Enter the year : \n");
	scanf("%d",&year);

	tdays=0;
	ndays=(year-1)*365;
	ldays=(year-1)/4-(year-1)/100+(year-1)/400;
	tdays=ldays+ndays;
	printf("Total days since 01/01/0001 are : %ld \n",tdays );
	tdays=tdays%7;
	if(tdays==0)
		printf("Monday\n");
	else if(tdays==1)
		printf("Tuesday\n");
	else if(tdays==2)
		printf("Wednesday\n");
	else if(tdays==3)
		printf("Thursday\n");
	else if(tdays==4)
		printf("Friday\n");
	else if(tdays==5)
		printf("Saturday\n");
	else if(tdays==6)
		printf("Sunday\n");	

}


/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 1d.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year : 
0001
Total days since 01/01/0001 are : 0 
Monday
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the year : 
2016
Total days since 01/01/0001 are : 735963 
Friday


*/