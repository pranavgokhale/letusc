/*If three side  of a triangle are entered find whether triangle is isosceles, equilateral,scalene or right angled triangle*/
#include <stdio.h>
main(){

	int a,b,c,greatest;
	printf("Enter the three sides of the triangle : \n");
	scanf("%d %d %d",&a,&b,&c);

	greatest=a>b?(a>c?a:c):(b>c?b:c);
	printf("Greatest side %d \n",greatest);
	if(a+b>c && c==greatest|| a+c>b && b==greatest|| b+c>a && a==greatest)
	{
		printf("Valid\n");
		if (a==b && b==c)
		{
			printf("Equilateral");
		}
		else if(a*a+b*b==c*c && c==greatest || c*c+b*b==a*a && a==greatest || a*a+c*c==b*b && b==greatest)
		{
			printf("Right angled triangle");
		}
		else if(a==b || b==c || a==c)
		{
			printf("Isosceles\n");
		}
		else
		{
			printf("Scalene");
		}
	}	

	else
		printf("Invalid triangle");
}

/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2g.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
2
2
2
Greatest side 2 
Valid
Equilateralpranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
2
2
6
Greatest side 6 
Invalid trianglepranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
2
2
3
Greatest side 3 
Valid
Isosceles
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the three sides of the triangle : 
3
4
5
Greatest side 5 
Valid
Right angled triangle
*/