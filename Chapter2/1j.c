/*Given three points write a program to check if all the three points fall on one straight line */
#include <stdio.h>
#include <math.h>
main()
{
	int x1,y1,x2,y2,x3,y3;
	float a,b;
	printf("Enter thte coordinates of the three points \n");
	scanf("%d %d %d %d %d %d",&x1,&y1,&x2,&y2,&x3,&y3);
	//if(abs((y1-y3)/(x1-x3))==abs((y1-y2)/(x1-x2)))---This logic goes crazy when denominator becomes 0
	//	printf("Collinear\n");
	//else
	//	printf("Non-collinear\n");
	a=(x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2; //area of a triangle--check if 0 to determine collinearty
	if(a==0)
		printf("Collinear\n");
	else
		printf("Non-collinear\n");
}