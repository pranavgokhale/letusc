/*A certain grade o f steel is graded according to the folloing conditions :
1)Hardness must be greater than 50
2)Carbon content must be less than 0.7 
3)Tensile strength must be greater than 5600
Find grade of steel from the conditions
*/
#include <stdio.h>
main()
{
	int c1,c2,c3,hardness,strength;
	float C;
	printf("enter the hardness, carbon content and tensile strength\n");
	scanf("%d %f %d",&hardness,&C,&strength);

	c1=hardness>50;
	c2=C<0.7;
	c3=strength>5600;

	if(c1 && c2 && c3)
		printf("Grade 10");
	else if(c1 && c2 && !c3)
		printf("Grade 9");
	else if(!c1 && c2 && c3)
		printf("Grade 8");
	else if(c1 && !c2 && c3)
		printf("Grade 7");
	else if((!c1 && !c2 && c3) || (!c1 && c2 && !c3) || (c1 && !c2 && !c3))//oly one condition true
		printf("Grade 6");
	else if(!c1 && !c2 && !c3)
		printf("Grade 5");
}


/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
51
0.6
5700
Grade 10pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
51
0.6
5500
Grade 9pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
38
0.5
6500
Grade 8pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
85
0.71
5800
Grade 7pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
74
0.8
5400
Grade 6pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
11
0.6
5800
Grade 8pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
11
0.8
5800
Grade 6pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
enter the hardness, carbon content and tensile strength
11
0.8
1000
Grade 5
*/