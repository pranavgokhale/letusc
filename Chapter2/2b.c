/*Any character is entered through the keyboard, write a program to determine whether the character entered  is a capital letter, a small case
letter, adigit or a special symbol.*/
#include <stdio.h>
main(){

	char a;
	printf("Enter a letter, number or symbol");
	scanf("%c",&a);

	if(a>=65 && a<=90)
		printf("Capital letter");
	else if(a>=97 && a<=122)
		printf("Small case letter");
	else if(a>=48 && a<=57)
		printf("Number ");
	else if(a>=0 && a<=47 || a>=58 && a<=64 ||a>=91 && a<=96 ||a>=123 && a<=127)
		printf("Special symbol");
}


/*output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2b.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter the letter or symbolA
Capital letterpranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2b.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter a letter, number or symbol@
Special symbolpranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter a letter, number or symbol5
Number pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 2b.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter a letter, number or symbola
Small case letterpranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter a letter, number or symbolQ
Capital letterpranav@Xerces:~/Profile/Let Us C/Chapter2$ 
*/