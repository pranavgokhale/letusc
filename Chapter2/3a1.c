/*Using conditional operators determine whether th character entered through the keyboard is a lower case alphabet or not */

#include <stdio.h>

main()
{
	char alphabet;
	printf("Enter any character to check if its lower case :\n");
	scanf("%c",&alphabet);

	alphabet>=97?(alphabet<=122?printf("Lower case alphabet \n "):printf("Not a lower case alphabet\n")):printf("Not a lower case alphabet\n");
}

/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 3a1.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter any character to check if its lower case :
a
Lower case alphabet 
 pranav@Xerces:~/Profile/Let Us C/Chapter2$ gcc 3a1.c 
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter any character to check if its lower case :
5
Not a lower case alphabet
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter any character to check if its lower case :
Q
Not a lower case alphabet
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter any character to check if its lower case :
z
Lower case alphabet 
*/