/*A five-digit number is entered through the keyboard. Write aprogram to obtain the reversed number 
and to determine whether the original and reversed numbers are equal (Basically check if palindrome)*/
#include <stdio.h>
main()
{
	int num,digit,sum,original;
	sum=0;
	printf("Enter the five-digit number : \n");
	scanf("%d",&num);
	original=num;
	digit=num%10;
	sum=sum+digit*10000;
	num=num/10;

	digit=num%10;
	sum=sum+digit*1000;
	num=num/10;

	digit=num%10;
	sum=sum+digit*100;
	num=num/10;

	digit=num%10;
	sum=sum+digit*10;
	num=num/10;

	digit=num%10;
	sum=sum+digit;
	num=num/10;

	printf("Reversed number : %d \n",sum );

	if(original==sum)
		printf("Palindrome \n");
	else 
		printf("Not a palindrome\n");
}
