/*An insurance company follows following rules to calculate premium. Write a program to output whether the person should be insured or not, his/her premium rate and
maximum amount for which he/she can be insured.*/

#include <stdio.h>
main()
{
	char health,place,sex;
	int age;

	printf("Enter your health status(E or P), age and whether you are from (V)illage or (C)ity and sex\n");
	scanf("%c %d %c %c",&health,&age,&place,&sex);

	if(health=='E' && age>=25 && age<=35 && place=='C' && sex=='M')
		printf("Premium is Rs. 4 per thousand and policy amount cannot exceed Rs 2 lakhs\n");

	else if(health=='E' && age>=25 && age<=35 && place=='C' && sex=='F')
		printf("Premium is Rs. 3 per thousand and policy amount cannot exceed Rs 1 lakhs\n");

	else if(health=='P' && age>=25 && age<=35 && place=='V' && sex=='M')
		printf("Premium is Rs. 6 per thousand and policy amount cannot exceed Rs 10,000\n");
	else 
		printf("Not insured\n");

} 

/*Output
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter your health status(E or P), age and whether you are from (V)illage or (C)ity and sex
P
40
V
M
Not insured
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter your health status(E or P), age and whether you are from (V)illage or (C)ity and sex
E
30
V
M
Not insured
pranav@Xerces:~/Profile/Let Us C/Chapter2$ ./a.out
Enter your health status(E or P), age and whether you are from (V)illage or (C)ity and sex
E
30
C
M
Premium is Rs. 4 per thousand and policy amount cannot exceed Rs 2 lakhs
*/